package kame.nekocore.inventory;

import org.bukkit.event.inventory.InventoryDragEvent;

/**
 * ドラッグ時のイベントを定義します。
 */
public interface DragEvent {
    /**
     * ドラッグ時のイベント
     *
     * @param event ドラッグイベント
     */
    void drag(InventoryDragEvent event);
}