package kame.nekocore.inventory;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Predicate;

/**
 * ActionInventoryの操作時に任意処理の実行を補助するクラスです。
 */
public class ActionItemStack extends ItemStack {
    private final List<ClickEvent> click = new ArrayList<>();
    private final List<DragEvent> drag = new ArrayList<>();

    /**
     * 種類と個数を指定してこのインスタンスを初期化します。
     *
     * @param material アイテムの種類
     * @param amount   アイテムの個数
     */
    public ActionItemStack(@NotNull Material material, int amount) {
        super(material, amount);
        setHideFlags();
    }

    /**
     * 種類を指定してこのインスタンスを初期化します。
     *
     * @param material アイテムの種類
     */
    public ActionItemStack(@NotNull Material material) {
        super(material);
        setHideFlags();
    }

    /**
     * 既存のItemStackからこのインスタンスを初期化します。
     *
     * @param item ItemStackオブジェクト
     */
    public ActionItemStack(@NotNull ItemStack item) {
        super(item);
        setHideFlags();
    }

    /**
     * 種類とアイテム名、説明文を指定してこのインスタンスを初期化します。
     *
     * @param material アイテムの種類
     * @param name     アイテムの名前
     * @param lore     アイテムの説明文
     */
    public ActionItemStack(@NotNull Material material, @Nullable String name, @NotNull String... lore) {
        super(material);
        var im = setHideFlags();
        Optional.ofNullable(im).ifPresent(x -> x.setDisplayName(name));
        Optional.ofNullable(im).ifPresent(x -> x.setLore(Arrays.asList(lore)));
        setItemMeta(im);
    }

    /**
     * このアイテムがクリックされた時のイベントを追加します。
     *
     * @param event 実行する処理
     */
    public void addClickEvent(ClickEvent event) {
        click.add(event);
    }

    /**
     * このアイテムがクリックされた時のイベントを追加します。
     *
     * @param event 実行する処理
     * @param filter 実行前に処理するフィルター
     */
    public void addClickEvent(ClickEvent event, Predicate<InventoryClickEvent> filter) {
        click.add(x -> {
            if (filter.test(x)) event.click(x);
        });
    }

    /**
     * このアイテムがクリックされた時の音を追加します。
     *
     * @param player 対象プレイヤー
     * @param sound  音の種類
     * @param volume 音の大きさ
     * @param pitch  音の高さ
     */
    public void addClickSound(@NotNull Player player, @NotNull Sound sound, float volume, float pitch) {
        addClickEvent(x -> player.playSound(player, sound, volume, pitch));
    }

    /**
     * このアイテムがクリックされた時の音を追加します。
     *
     * @param player 対象プレイヤー
     * @param sound  音の種類
     * @param volume 音の大きさ
     * @param pitch  音の高さ
     * @param filter 実行前に処理するフィルター
     */
    public void addClickSound(@NotNull Player player, @NotNull Sound sound, float volume, float pitch, @NotNull Predicate<InventoryClickEvent> filter) {
        addClickEvent(x -> player.playSound(player, sound, volume, pitch), filter);
    }

    /**
     * このアイテムに全てのHideFlagを付与します。
     *
     * @return ItemMeta
     */
    @Nullable
    public ItemMeta setHideFlags() {
        var im = getItemMeta();
        Optional.ofNullable(im).ifPresent(x -> x.addItemFlags(ItemFlag.values()));
        setItemMeta(im);
        return im;
    }

    /**
     * このアイテムから全てのHideFlagを削除します。
     *
     * @return ItemMeta
     */
    @Nullable
    public ItemMeta removeHideFlags() {
        var im = getItemMeta();
        Optional.ofNullable(im).ifPresent(x -> x.removeItemFlags(ItemFlag.values()));
        setItemMeta(im);
        return im;
    }

    /**
     * このアイテムから指定したHideFlagを削除します。
     *
     * @param flags 削除するフラグ
     * @return ItemMeta
     */
    @Nullable
    public ItemMeta removeHideFlags(ItemFlag... flags) {
        var im = getItemMeta();
        Optional.ofNullable(im).ifPresent(x -> x.removeItemFlags(flags));
        setItemMeta(im);
        return im;
    }

    /**
     * このアイテムがドラッグされた時のイベントを追加します。
     *
     * @param event 実行する処理
     */
    public void addDragEvent(DragEvent event) {
        drag.add(event);
    }

    /**
     * このアイテムを発光状態(エンチャント)にします。
     */
    public void setGlow() {
        addUnsafeEnchantment(Enchantment.LUCK, 0);
    }

    /**
     * このアイテムの発光状態(エンチャント)を設定します。
     *
     * @param isGlow trueを指定した場合は発光、falseを指定した場合は非発光
     */
    public void setGlow(boolean isGlow) {
        if (isGlow) {
            addUnsafeEnchantment(Enchantment.LUCK, 0);
        } else {
            for (var enchantments : getEnchantments().keySet()) {
                removeEnchantment(enchantments);
            }
        }
    }

    /**
     * このアイテムの名前を設定します。
     *
     * @param name アイテムに設定する文字列
     */
    public void setDisplayName(String name) {
        var im = super.getItemMeta();
        Optional.ofNullable(im).ifPresent(x -> x.setDisplayName(name));
        super.setItemMeta(im);
    }

    /**
     * このアイテムのローカライズ名を設定します。
     *
     * @param name アイテムに設定する文字列
     */
    public void setLocalizedName(String name) {
        var im = super.getItemMeta();
        Optional.ofNullable(im).ifPresent(x -> x.setLocalizedName(name));
        super.setItemMeta(im);
    }

    /**
     * このアイテムの説明文を設定します。
     *
     * @param name アイテムに設定する文字列の可変長配列
     */
    public void setLore(String... name) {
        setLore(Arrays.asList(name));
    }

    /**
     * このアイテムの説明文を設定します。
     *
     * @param name アイテムに設定する文字列のコレクション
     */
    public void setLore(Collection<String> name) {
        var im = super.getItemMeta();
        Optional.ofNullable(im).ifPresent(x -> x.setLore(new ArrayList<>(name)));
        super.setItemMeta(im);
    }

    /**
     * このアイテムの説明文を末尾に追加します。
     *
     * @param name アイテムに設定する文字列の可変長配列
     */
    public void addLore(String... name) {
        addLore(Arrays.asList(name));
    }

    /**
     * このアイテムの説明文を末尾に追加します。
     *
     * @param name アイテムに設定する文字列のコレクション
     */
    public void addLore(@NotNull Collection<String> name) {
        var im = super.getItemMeta();
        if (im != null && im.getLore() != null) {
            var lore = im.getLore();
            lore.addAll(name);
            im.setLore(lore);
            super.setItemMeta(im);
        }
    }

    @Override
    public String toString() {
        return "ActionItemStack{" + super.toString() + "}";
    }

    @NotNull
    Iterable<ClickEvent> getClickEvents() {
        return click;
    }

    @NotNull
    Iterable<DragEvent> getDragEvents() {
        return drag;
    }

}
