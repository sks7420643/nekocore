package kame.nekocore.inventory;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.IntStream;

/**
 * インベントリGUIの機能を提供するクラスです。
 */
public class ActionInventory {

    private final String title;
    private final ActionHolder holder;
    private final ItemStack[] items;
    private final List<ClickEvent> click = new ArrayList<>();
    private final List<DragEvent> drag = new ArrayList<>();
    private final List<CloseEvent> close = new ArrayList<>();

    /**
     * タイトルとインベントリサイズからこのインスタンスを初期化します。
     *
     * @param title インベントリタイトル
     * @param size  インベントリサイズ [9,18,27,36,45,54]のいずれか
     */
    public ActionInventory(@NotNull String title, int size) {
        this.title = title;
        this.items = new ItemStack[size <= 9 ? 9 : size <= 18 ? 18 : size <= 27 ? 27 : size <= 36 ? 36 : size <= 45 ? 45 : 54];
        this.holder = new ActionHolder(this);
    }

    /**
     * 指定の位置にあるアイテムを取得します。
     *
     * @param index インベントリスロット
     * @return インベントリスロットにあるアイテム
     */
    @Nullable
    public ItemStack getItem(int index) {
        return items[index];
    }

    /**
     * 指定のスロットにアイテムを設定します。
     *
     * @param index 設定するスロット
     * @param item  設定するアイテム
     */
    public void setItem(int index, @Nullable ItemStack item) {
        holder.getInventory().setItem(index, items[index] = item);
    }

    /**
     * インベントリのタイトルを取得します。
     *
     * @return インベントリのタイトル
     */
    @NotNull
    public String getTitle() {
        return title;
    }

    /**
     * 指定のスロットに名前と説明文を編集したアイテムを設定します。
     * @param index 設定するスロット
     * @param item  設定するアイテム
     * @param name  アイテムに設定する名前
     * @param lore  アイテムに設定する説明文
     */
    public void setMetaItem(int index, ItemStack item, String name, String... lore) {
        var im = item.getItemMeta();
        if (im != null) {
            im.setDisplayName(name);
            im.setLore(Arrays.asList(lore));
            im.addItemFlags(ItemFlag.values());
            item.setItemMeta(im);
        }
        setItem(index, item);
    }

    /**
     * 引数で指定した対象にインベントリを開かせます。
     *
     * @param player インベントリを開かせる対象
     * @return インベントリが開かれた場合はそのInventoryView
     */
    @Nullable
    public InventoryView openInventory(HumanEntity player) {
        return player.openInventory(holder.getInventory());
    }

    @Nullable
    private ItemStack setGlow(@Nullable ItemStack item, boolean glow) {
        if (item == null) {
            return null;
        }
        var ret = item.clone();
        if (glow) {
            ret.addUnsafeEnchantment(Enchantment.LUCK, 0);
        } else {
            ret.getEnchantments().keySet().forEach(ret::removeEnchantment);
        }
        return ret;
    }

    private void update() {
        IntStream.of(0, items.length).forEach(x -> holder.getInventory().setItem(x, items[x]));
    }

    /**
     * インベントリの指定スロットのアイテムを光らせた状態(エンチャント)にします。
     * 指定以外のスロットのは光ってない状態になります。
     *
     * @param slot 光らせるスロット
     */
    public void setGlow(Integer... slot) {
        setGlow(Set.of(slot));
    }

    /**
     * インベントリの指定スロットのアイテムを光らせた状態(エンチャント)にします。
     * 指定以外のスロットのは光ってない状態になります。
     *
     * @param slot 光らせるスロット
     */
    public void setGlow(Collection<Integer> slot) {
        IntStream.range(0, items.length).forEach(x -> items[x] = setGlow(items[x], slot.contains(x)));
        update();
    }

    /**
     * インベントリの指定スロットのアイテムを光らせた状態(エンチャント)にします。
     * 指定以外のスロットはメソッド呼び出し前の状態が維持されます。
     *
     * @param slot 光らせるスロット
     */
    public void addGlow(Integer... slot) {
        addGlow(Set.of(slot));
    }

    /**
     * インベントリの指定スロットのアイテムを光らせた状態(エンチャント)にします。
     * 指定以外のスロットはメソッド呼び出し前の状態が維持されます。
     *
     * @param slot 光らせるスロット
     */
    public void addGlow(Collection<Integer> slot) {
        slot.forEach(x -> items[x] = setGlow(items[x], true));
        update();
    }

    /**
     * インベントリでActionItemStack以外のアイテムがクリックされた時のイベントを追加します。
     *
     * @param event 実行するイベント
     */
    public void addClickEvent(ClickEvent event) {
        click.add(event);
    }

    /**
     * インベントリでActionItemStack以外のアイテムがドラッグされた時のイベントを追加します。
     *
     * @param event 実行するイベント
     */
    public void addDragEvent(DragEvent event) {
        drag.add(event);
    }

    /**
     * インベントリが何らかの操作で閉じられたときに発生するイベントを追加します。
     *
     * @param event 実行するイベント
     */
    public void addCloseEvent(CloseEvent event) {
        close.add(event);
    }

    /**
     * インベントリのサイズを取得します。
     *
     * @return インベントリのサイズ
     */
    public int getSize() {
        return items.length;
    }

    Iterable<ClickEvent> getClickEvents() {
        return click;
    }

    Iterable<DragEvent> getDragEvents() {
        return drag;
    }

    Iterable<CloseEvent> getCloseEvents() {
        return close;
    }
}
