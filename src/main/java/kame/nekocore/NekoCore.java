package kame.nekocore;

import kame.nekocore.inventory.ActionInventoryListener;
import kame.nekocore.update.PluginUpdater;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class NekoCore extends JavaPlugin {

    private final Logger logger = getLogger();

    @Override
    public void onEnable() {
        saveDefaultConfig();
        if (checkUpdate()) return;
        Bukkit.getPluginManager().registerEvents(ActionInventoryListener.instance, this);
    }

    private boolean checkUpdate() {
        var config = Objects.requireNonNull(getConfig().getConfigurationSection("update"));
        if (!config.getBoolean("checkUpdate")) return false;
        var autoUpdate = config.getBoolean("autoUpdate");
        logger.info("Checking update...");
        try {
            var update = PluginUpdater.check(this, 54021868, "kame/NekoCore");
            if (update.isPresent()) {
                var pack = update.get();
                var updated_at = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(pack.updated_at());
                logger.info("A new version is available! [%s: updated at:%s]".formatted(pack.version(), updated_at));
                if (!autoUpdate) return false;
                logger.info("Downloading... %s".formatted(getFile().getName()));
                PluginUpdater.download(pack, getFile());
                logger.info("Download completed!");
                var loader = Bukkit.getPluginManager();
                loader.disablePlugin(this);
                Optional.ofNullable(loader.loadPlugin(getFile())).ifPresent(loader::enablePlugin);
                return true;
            } else {
                logger.info("Already up to date");
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, "Failed to update the plugin.", e);
        }
        return false;
    }

    @Override
    public void onDisable() {
    }
}
