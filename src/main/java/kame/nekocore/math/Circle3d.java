package kame.nekocore.math;

import org.bukkit.Location;

/**
 * 3次元の円形の計算に必要な機能を提供するレコードクラスです。
 *
 * @param x      X座標
 * @param y      Y座標
 * @param z      Z座標
 * @param radius 円の半径
 * @param pitch  円の向き
 * @param yaw    円の向き
 */
public record Circle3d(double x, double y, double z, double radius, double pitch, double yaw) {

    /**
     * 中心位置と半径、向きからこのインスタンスを初期化します。
     *
     * @param center 中心座標
     * @param radius 円の半径
     * @param pitch  円の向き
     * @param yaw    円の向き
     */
    public Circle3d(Point3d center, double radius, double pitch, double yaw) {
        this(center.x(), center.y(), center.z(), radius, pitch, yaw);
    }

    /**
     * 中心位置と半径からこのインスタンスを初期化します。
     *
     * @param center 中心座標
     * @param radius 円の半径
     */
    public Circle3d(Point3d center, double radius) {
        this(center, radius, 0, 0);
    }

    /**
     * 中心位置と半径、向きからこのインスタンスを初期化します。
     *
     * @param loc    中心座標(向きを含む)
     * @param radius 円の半径
     */
    public Circle3d(Location loc, double radius) {
        this(loc.getX(), loc.getY(), loc.getZ(), radius, loc.getPitch(), loc.getYaw());
    }

    /**
     * この円の指定角度にある位置の座標を取得します。
     *
     * @param degree 取得する座標の回転角度
     * @return 回転角度から求められた座標
     */
    public Point3d getPoint(double degree) {
        return Orientation.rotate(new Point3d(0, radius, 0), Point3d.ZERO, yaw, pitch, degree).add(x, y, z);
    }
}
