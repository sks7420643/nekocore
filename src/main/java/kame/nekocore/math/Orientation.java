package kame.nekocore.math;

import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

/**
 * このクラスは座標回転の機能を提供するクラスです。
 */
public class Orientation {

    private Orientation() throws IllegalAccessException {
        throw new IllegalAccessException();
    }

    /**
     * 第一引数に渡された値を第2引数を中心に回転させた座標値を取得します。
     *
     * @param point  回転させる座標値
     * @param center 回転中心となる座標値
     * @param yaw    Y軸回転[degree]
     * @param pitch  X軸回転[degree]
     * @param roll   Z軸回転[degree]
     * @return 回転後の座標値
     */
    @NotNull
    public static Point3d rotate(@NotNull Point3d point, @NotNull Point3d center, double yaw, double pitch, double roll) {
        return new Point3d(rotate(point.toVector(), center.x(), center.y(), center.z(), yaw, pitch, roll));
    }

    /**
     * 第一引数に渡された値を第2引数を中心に回転させます。
     *
     * @param vector 回転させるベクトル(渡された値は内部で変更されます)
     * @param center 回転中心となるベクトル
     * @param yaw    Y軸回転[degree]
     * @param pitch  X軸回転[degree]
     * @param roll   Z軸回転[degree]
     * @return 回転後のベクトル(第一引数で渡された値と同じオブジェクト)
     */
    @NotNull
    public static Vector rotate(@NotNull Vector vector, @NotNull Vector center, double yaw, double pitch, double roll) {
        return rotate(vector, center.getX(), center.getY(), center.getZ(), yaw, pitch, roll);
    }

    /**
     * 第一引数に渡された値を指定座標を中心に回転させます。
     *
     * @param vector 回転させるベクトル(渡された値は内部で変更されます)
     * @param x      回転中心となるX座標値
     * @param y      回転中心となるY座標値
     * @param z      回転中心となるZ座標値
     * @param yaw    Y軸回転[degree]
     * @param pitch  X軸回転[degree]
     * @param roll   Z軸回転[degree]
     * @return 回転後のベクトル(第一引数で渡された値と同じオブジェクト)
     */
    @NotNull
    public static Vector rotate(@NotNull Vector vector, double x, double y, double z, double yaw, double pitch, double roll) {
        vector.setX(vector.getX() - x).setY(vector.getY() - y).setZ(vector.getZ() - z);
        if (roll != 0) {
            double sin = Math.sin(Math.toRadians(roll));
            double cos = Math.cos(Math.toRadians(roll));
            double tmp = vector.getX();
            vector.setX(tmp * cos - vector.getY() * sin).setY(tmp * sin + vector.getY() * cos);
        }
        if (pitch != 0) {
            double sin = Math.sin(Math.toRadians(pitch));
            double cos = Math.cos(Math.toRadians(pitch));
            double tmp = vector.getY();
            vector.setY(tmp * cos - vector.getZ() * sin).setZ(tmp * sin + vector.getZ() * cos);
        }
        if (yaw != 0) {
            double sin = Math.sin(Math.toRadians(yaw));
            double cos = Math.cos(Math.toRadians(yaw));
            double tmp = vector.getX();
            vector.setX(tmp * cos - vector.getZ() * sin).setZ(tmp * sin + vector.getZ() * cos);
        }
        return vector.setX(vector.getX() + x).setY(vector.getY() + y).setZ(vector.getZ() + z);
    }

    /**
     * 第一引数に渡された値を第2引数を中心に復元方向に回転させた座標値を取得します。
     *
     * @param point  回転させる座標値
     * @param center 回転中心となる座標値
     * @param yaw    Y軸回転[degree]
     * @param pitch  X軸回転[degree]
     * @param roll   Z軸回転[degree]
     * @return 回転後の座標値
     */
    @NotNull
    public static Point3d rotateReverse(@NotNull Point3d point, @NotNull Point3d center, double yaw, double pitch, double roll) {
        return new Point3d(rotateReverse(point.toVector(), center.x(), center.y(), center.z(), yaw, pitch, roll));
    }

    /**
     * 第一引数に渡された値を第2引数を中心に復元方向に回転させます。
     *
     * @param vector 回転させるベクトル(渡された値は内部で変更されます)
     * @param center 回転中心となるベクトル
     * @param yaw    Y軸回転[degree]
     * @param pitch  X軸回転[degree]
     * @param roll   Z軸回転[degree]
     * @return 回転後の座標値
     */
    @NotNull
    public static Vector rotateReverse(@NotNull Vector vector, @NotNull Vector center, double yaw, double pitch, double roll) {
        return rotateReverse(vector, center.getX(), center.getY(), center.getZ(), yaw, pitch, roll);
    }

    /**
     * 第一引数に渡された値を第2引数を中心に復元方向に回転させます。
     *
     * @param vector 回転させるベクトル(渡された値は内部で変更されます)
     * @param x      回転中心となるX座標値
     * @param y      回転中心となるY座標値
     * @param z      回転中心となるZ座標値
     * @param yaw    Y軸回転[degree]
     * @param pitch  X軸回転[degree]
     * @param roll   Z軸回転[degree]
     * @return 回転後の座標値
     */
    @NotNull
    public static Vector rotateReverse(@NotNull Vector vector, double x, double y, double z, double yaw, double pitch, double roll) {
        vector.setX(vector.getX() - x).setY(vector.getY() - y).setZ(vector.getZ() - z);
        if (yaw != 0) {
            double sin = Math.sin(Math.toRadians(-yaw));
            double cos = Math.cos(Math.toRadians(-yaw));
            double tmp = vector.getX();
            vector.setX(tmp * cos - vector.getZ() * sin).setZ(tmp * sin + vector.getZ() * cos);
        }
        if (pitch != 0) {
            double sin = Math.sin(Math.toRadians(-pitch));
            double cos = Math.cos(Math.toRadians(-pitch));
            double tmp = vector.getY();
            vector.setY(tmp * cos - vector.getZ() * sin).setZ(tmp * sin + vector.getZ() * cos);
        }
        if (roll != 0) {
            double sin = Math.sin(Math.toRadians(-roll));
            double cos = Math.cos(Math.toRadians(-roll));
            double tmp = vector.getX();
            vector.setX(tmp * cos - vector.getY() * sin).setY(tmp * sin + vector.getY() * cos);
        }
        return vector.setX(vector.getX() + x).setY(vector.getY() + y).setZ(vector.getZ() + z);
    }
}
