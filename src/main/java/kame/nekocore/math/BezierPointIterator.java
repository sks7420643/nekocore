package kame.nekocore.math;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 3次ベジェ曲線での補間シーケンスを定義するクラスです。
 */
public class BezierPointIterator implements Iterator<Point3d> {

    private final Bezier3d bezier;
    private final double[] lengths;
    private final int size;
    private final double rate;
    private int cursor;

    /**
     * ベジェ曲線と分割数、等間隔係数からこのインスタンスを初期化します。
     *
     * @param bezier ベジェ曲線のオブジェクト
     * @param n      分割数
     * @param c      等間隔係数 (0.0は間隔補正無し、1.0で等間隔補正あり)
     * @throws IllegalArgumentException 引数 n または c の値が負の数である場合に発生
     */
    public BezierPointIterator(@NotNull Bezier3d bezier, int n, double c) {
        if (n < 0) throw new IllegalArgumentException("n");
        if (c < 0) throw new IllegalArgumentException("c");
        this.bezier = bezier;
        size = n - 1;
        rate = c;
        lengths = new double[n];
        var point = bezier.lerpBezier(0);
        for (var i = 1; i < lengths.length; i++) {
            lengths[i] = lengths[i - 1] + point.distance(point = bezier.lerpBezier(i / (lengths.length - 1.0)));
        }
    }

    /**
     * 初期化時に使用されたベジェ曲線の長さを取得します。
     * この呼び出しで取得される長さの精度は分割数に依存します。
     *
     * @return 初期化時に使用されたベジェ曲線の長さ
     */
    public double getLength() {
        return lengths[lengths.length - 1];
    }

    /**
     * 現在のシーケンスに次の要素があるかを返します。
     *
     * @return 次の要素がある場合は true、そうでない場合は false
     */
    @Override
    public boolean hasNext() {
        return cursor <= size;
    }

    /**
     * 現在のシーケンスの次の要素を取得します。
     *
     * @return 次の要素
     * @throws NoSuchElementException 次の要素が無い状態で呼び出された場合に発生。
     */
    @Override
    public Point3d next() {
        if (cursor > size) throw new NoSuchElementException();
        var pos = cursor / (double) size;
        var len = lengths[lengths.length - 1];
        var dis = pos * len * rate + (1 - rate) * lengths[cursor++];
        int i = 0;
        while (i < lengths.length - 1) {
            if (dis <= lengths[i + 1]) break;
            i++;
        }
        return bezier.lerpBezier((i + (dis - lengths[i]) / (lengths[i + 1] - lengths[i])) / size);
    }
}
