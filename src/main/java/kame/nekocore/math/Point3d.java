package kame.nekocore.math;

import org.bukkit.Location;
import org.bukkit.util.Vector;

/**
 * 3次元座標の計算に必要な機能を提供するレコードクラスです。
 *
 * @param x X座標値
 * @param y Y座標値
 * @param z Z座標値
 */
public record Point3d(double x, double y, double z) {
    /**
     * ゼロ地点のPoint3dオブジェクトを取得します。
     */
    public static final Point3d ZERO = new Point3d(0, 0, 0);

    /**
     * Vectorオブジェクトからこのインスタンスを初期化します。
     *
     * @param vec Vectorオブジェクト
     */
    public Point3d(Vector vec) {
        this(vec.getX(), vec.getY(), vec.getZ());
    }

    /**
     * Locationオブジェクトからこのインスタンスを初期化します。
     *
     * @param loc Locationオブジェクト
     */
    public Point3d(Location loc) {
        this(loc.getX(), loc.getY(), loc.getZ());
    }

    /**
     * ゼロ地点を中心に現在の座標の各軸回転させた座標を取得します。
     *
     * @param yaw   yaw回転させる角度(degree)
     * @param pitch pitch回転させる角度(degree)
     * @param roll  roll回転させる角度(degree)
     * @return 回転後の座標
     */
    public Point3d rotate(double yaw, double pitch, double roll) {
        return Orientation.rotate(this, Point3d.ZERO, yaw, pitch, roll);
    }

    /**
     * 2点間の距離を求めます。
     *
     * @param other 現在の座標から距離を測る座標
     * @return 2点間の距離
     */
    public double distance(Point3d other) {
        return Math.sqrt(distanceSquared(other));
    }

    /**
     * 2点間の二乗距離を求めます。
     *
     * @param other 現在の座標から距離を測る座標
     * @return 2点間の二乗距離
     */
    public double distanceSquared(Point3d other) {
        return Math.pow(x - other.x, 2) + Math.pow(y - other.y, 2) + Math.pow(z - other.z, 2);
    }

    /**
     * 現在の座標に引数の座標値を加算した結果を返します。
     *
     * @param x 加算する座標値X
     * @param y 加算する座標値Y
     * @param z 加算する座標値Z
     * @return 現在の座標と引数の値を足した座標値
     */
    public Point3d add(double x, double y, double z) {
        return new Point3d(this.x + x, this.y + y, this.z + z);
    }

    /**
     * 現在の座標に引数の座標値を加算した結果を返します。
     *
     * @param other 加算する座標値
     * @return 現在の座標と引数の値を足した座標値
     */
    public Point3d add(Point3d other) {
        return add(other.x, other.y, other.z);
    }

    /**
     * 現在の座標に引数の座標値を加算した結果を返します。
     *
     * @param other 加算する座標値
     * @return 現在の座標と引数の値を足した座標値
     */
    public Point3d add(Location other) {
        return add(other.getX(), other.getY(), other.getZ());
    }

    /**
     * 現在の座標に引数の座標値を加算した結果を返します。
     *
     * @param other 加算する座標値
     * @return 現在の座標と引数の値を足した座標値
     */
    public Point3d add(Vector other) {
        return add(other.getX(), other.getY(), other.getZ());
    }

    /**
     * 現在の座標に引数の座標値を減算した結果を返します。
     *
     * @param x 減算する座標値X
     * @param y 減算する座標値Y
     * @param z 減算する座標値Z
     * @return 現在の座標と引数の値を引いた座標値
     */
    public Point3d subtract(double x, double y, double z) {
        return new Point3d(this.x - x, this.y - y, this.z - z);
    }

    /**
     * 現在の座標に引数の座標値を減算した結果を返します。
     *
     * @param other 減算する座標値
     * @return 現在の座標と引数の値を引いた座標値
     */
    public Point3d subtract(Point3d other) {
        return subtract(other.x, other.y, other.z);
    }

    /**
     * 現在の座標に引数の座標値を減算した結果を返します。
     *
     * @param other 減算する座標値
     * @return 現在の座標と引数の値を引いた座標値
     */
    public Point3d subtract(Location other) {
        return subtract(other.getX(), other.getY(), other.getZ());
    }

    /**
     * 現在の座標に引数の座標値を減算した結果を返します。
     *
     * @param other 減算する座標値
     * @return 現在の座標と引数の値を引いた座標値
     */
    public Point3d subtract(Vector other) {
        return subtract(other.getX(), other.getY(), other.getZ());
    }

    /**
     * 現在の座標に引数の座標値を乗算した結果を返します。
     *
     * @param x 乗算する座標値X
     * @param y 乗算する座標値Y
     * @param z 乗算する座標値Z
     * @return 現在の座標と引数の値をかけた座標値
     */
    public Point3d multiply(double x, double y, double z) {
        return new Point3d(this.x * x, this.y * y, this.z * z);
    }

    /**
     * 現在の座標に引数の座標値を乗算した結果を返します。
     *
     * @param m 乗算する値
     * @return 現在の座標と引数の値をかけた座標値
     */
    public Point3d multiply(double m) {
        return multiply(m, m, m);
    }

    /**
     * 現在の座標に引数の座標値を除算した結果を返します。
     *
     * @param x 除算する座標値X
     * @param y 除算する座標値Y
     * @param z 除算する座標値Z
     * @return 現在の座標と引数の値を割った座標値
     */
    public Point3d divide(double x, double y, double z) {
        return new Point3d(this.x / x, this.y / y, this.z / z);
    }

    /**
     * 現在の座標に引数の座標値を除算した結果を返します。
     *
     * @param d 除算する値
     * @return 現在の座標と引数の値を割った座標値
     */
    public Point3d divide(double d) {
        return divide(d, d, d);
    }

    /**
     * 現在の座標値からVectorに変換します。
     *
     * @return 変換された値
     */
    public Vector toVector() {
        return new Vector(x, y, z);
    }
}
