package kame.nekocore.collision;

import kame.nekocore.math.Line3d;
import kame.nekocore.math.Orientation;
import kame.nekocore.math.Point3d;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.stream.IntStream;

/**
 * 各軸の回転が可能な衝突計算用の機能を提供するクラスです。
 */
public class OrientedBoundingBox {
    private static final Vector ZERO_VECTOR = new Vector();
    private final BoundingBox aabb;
    private final Vector center = new Vector();
    private double yaw;
    private double pitch;
    private double roll;

    /**
     * 2点を対角とするcenterを中心に回転させたOrientedBoundingBoxインスタンスを作成します。
     *
     * @param pos1   対角1
     * @param pos2   対角2
     * @param center 回転中心(roll以外の角度含む)
     */
    public OrientedBoundingBox(Location pos1, Location pos2, Location center) {
        this(pos1.getX(), pos1.getY(), pos1.getZ(), pos2.getX(), pos2.getY(), pos2.getZ());
        rotate(center.getX(), center.getY(), center.getZ(), center.getYaw(), center.getPitch(), 0);
    }

    /**
     * 2点を対角とするcenterを中心に回転させたOrientedBoundingBoxインスタンスを作成します。
     *
     * @param pos1   対角1
     * @param pos2   対角2
     * @param center 回転中心
     * @param yaw    Y軸回転
     * @param pitch  X軸回転
     * @param roll   Z軸回転
     */
    public OrientedBoundingBox(Location pos1, Location pos2, Location center, double yaw, double pitch, double roll) {
        this(pos1.getX(), pos1.getY(), pos1.getZ(), pos2.getX(), pos2.getY(), pos2.getZ());
        rotate(center.getX(), center.getY(), center.getZ(), yaw, pitch, yaw);
    }

    /**
     * 2点を対角とする回転中心を中心に回転させたOrientedBoundingBoxインスタンスを作成します。
     *
     * @param x1    対角1 X座標値
     * @param y1    対角1 Y座標値
     * @param z1    対角1 Z座標値
     * @param x2    対角1 X座標値
     * @param y2    対角1 Y座標値
     * @param z2    対角1 Z座標値
     * @param x3    回転中心 X座標値
     * @param y3    回転中心 Y座標値
     * @param z3    回転中心 Z座標値
     * @param yaw   Y軸回転
     * @param pitch X軸回転
     * @param roll  Z軸回転
     */
    public OrientedBoundingBox(double x1, double y1, double z1, double x2, double y2, double z2,
                               double x3, double y3, double z3, double yaw, double pitch, double roll) {
        this(x1, y1, z1, x2, y2, z2);
        rotate(x3, y3, z3, yaw, pitch, roll);
    }

    /**
     * 2点を対角とする未回転のOrientedBoundingBoxインスタンスを作成します。
     *
     * @param x1 対角1 X座標値
     * @param y1 対角1 Y座標値
     * @param z1 対角1 Z座標値
     * @param x2 対角1 X座標値
     * @param y2 対角1 Y座標値
     * @param z2 対角1 Z座標値
     */
    public OrientedBoundingBox(double x1, double y1, double z1, double x2, double y2, double z2) {
        this.aabb = new BoundingBox(x1, y1, z1, x2, y2, z2);
    }

    /**
     * このオブジェクトをリサイズします。
     *
     * @param x1 X座標値1
     * @param y1 Y座標値1
     * @param z1 Z座標値1
     * @param x2 X座標値2
     * @param y2 Y座標値2
     * @param z2 Z座標値2
     * @return このオブジェクトの参照
     */
    @NotNull
    public OrientedBoundingBox resize(double x1, double y1, double z1, double x2, double y2, double z2) {
        aabb.resize(x1, y1, z1, x2, y2, z2);
        return this;
    }


    /**
     * このオブジェクトの回転を再設定します。
     *
     * @param x     回転中心X座標
     * @param y     回転中心Y座標
     * @param z     回転中心Z座標
     * @param yaw   Y軸回転量[degree]
     * @param pitch X軸回転量[degree]
     * @param roll  Z軸回転量[degree]
     * @return このオブジェクトの参照
     */
    @NotNull
    public OrientedBoundingBox rotate(double x, double y, double z, double yaw, double pitch, double roll) {
        center.setX(x).setY(y).setZ(z);
        this.yaw = yaw;
        this.pitch = pitch;
        this.roll = roll;
        return this;
    }

    /**
     * このオブジェクトに対しての衝突判定を行います。
     *
     * @param start       開始位置
     * @param direction   方向ベクトル
     * @param maxDistance 最大距離
     * @return 衝突した場合は衝突位置、衝突しない場合 null
     */
    @Nullable
    public RayTraceResult rayTrace(@NotNull Vector start, @NotNull Vector direction, double maxDistance) {
        start = Orientation.rotateReverse(start.clone(), center, yaw, pitch, roll);
        direction = Orientation.rotateReverse(direction.clone(), ZERO_VECTOR, yaw, pitch, roll);
        var result = Optional.ofNullable(aabb.rayTrace(start, direction, maxDistance));
        var hit = result.map(x -> Orientation.rotate(x.getHitPosition(), center, yaw, pitch, roll));
        return hit.map(x -> new RayTraceResult(x, result.get().getHitBlockFace())).orElse(null);
    }

    /**
     * このオブジェクトの輪郭を描画します。
     *
     * @param player   ターゲットとするプレイヤー
     * @param particle 描画パーティクル種類
     * @param split    1辺あたりの描画点数
     * @param extra　　 パーティクルのextra
     * @param data     パーティクルのdata
     */
    public void drawBox(@NotNull Player player, @NotNull Particle particle, int split, double extra, @Nullable Object data) {
        var min = aabb.getMin();
        var max = aabb.getMax();

        var pos0 = Orientation.rotate(new Vector(min.getX(), min.getY(), min.getZ()), center, yaw, pitch, roll); // 0,1 0,2 0,4
        var pos1 = Orientation.rotate(new Vector(max.getX(), min.getY(), min.getZ()), center, yaw, pitch, roll); // 1,3 1,5
        var pos2 = Orientation.rotate(new Vector(min.getX(), max.getY(), min.getZ()), center, yaw, pitch, roll); // 2,4
        var pos3 = Orientation.rotate(new Vector(max.getX(), max.getY(), min.getZ()), center, yaw, pitch, roll); //
        var pos4 = Orientation.rotate(new Vector(min.getX(), min.getY(), max.getZ()), center, yaw, pitch, roll); //
        var pos5 = Orientation.rotate(new Vector(max.getX(), min.getY(), max.getZ()), center, yaw, pitch, roll); // 5,3
        var pos6 = Orientation.rotate(new Vector(min.getX(), max.getY(), max.getZ()), center, yaw, pitch, roll); // 6,4 6,2
        var pos7 = Orientation.rotate(new Vector(max.getX(), max.getY(), max.getZ()), center, yaw, pitch, roll); // 7,6 7,5 7,3

        drawLine(player, particle, new Line3d(pos0, pos1), split, extra, data);
        drawLine(player, particle, new Line3d(pos0, pos2), split, extra, data);
        drawLine(player, particle, new Line3d(pos0, pos4), split, extra, data);
        drawLine(player, particle, new Line3d(pos1, pos3), split, extra, data);
        drawLine(player, particle, new Line3d(pos1, pos5), split, extra, data);
        drawLine(player, particle, new Line3d(pos2, pos3), split, extra, data);
        drawLine(player, particle, new Line3d(pos5, pos4), split, extra, data);
        drawLine(player, particle, new Line3d(pos6, pos4), split, extra, data);
        drawLine(player, particle, new Line3d(pos6, pos2), split, extra, data);
        drawLine(player, particle, new Line3d(pos7, pos6), split, extra, data);
        drawLine(player, particle, new Line3d(pos7, pos5), split, extra, data);
        drawLine(player, particle, new Line3d(pos7, pos3), split, extra, data);
    }

    private void drawLine(@NotNull Player player, @NotNull Particle particle, @NotNull Line3d line, int split, double extra, @Nullable Object data) {
        var points = IntStream.range(0, split + 1).mapToObj(x -> line.lerp(x / (double) split)).toArray(Point3d[]::new);
        for (var point : points) {
            player.spawnParticle(particle, point.x(), point.y(), point.z(), 1, 0, 0, 0, extra, data);
        }
    }

}
