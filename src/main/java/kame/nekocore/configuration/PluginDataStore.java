package kame.nekocore.configuration;

import kame.nekocore.NekoCore;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * プラグインの内部利用データを保持する機能を提供します。
 */
public class PluginDataStore {

    private static final YamlConfiguration store;
    private static final File file;

    static {
        var plugin = JavaPlugin.getPlugin(NekoCore.class);
        file = new File(plugin.getDataFolder(), "plugin.save");
        store = file.isFile() ? YamlConfiguration.loadConfiguration(file) : createNew();
    }

    private static YamlConfiguration createNew() {
        try {
            var store = new YamlConfiguration();
            store.save(file);
            return store;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 最終更新時刻をLong型で取得します。
     *
     * @return Long型の日時
     */
    public static long getLastUpdated() {
        return store.getLong("LastUpdated");
    }

    /**
     * 最終更新日時を設定します。
     *
     * @param lastUpdated 最終更新日時
     * @throws IOException データの保存に失敗したとき
     */
    public static void setLastUpdated(long lastUpdated) throws IOException {
        store.set("LastUpdated", lastUpdated);
        store.save(file);
    }
}
