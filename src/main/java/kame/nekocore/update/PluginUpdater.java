package kame.nekocore.update;

import kame.repositories.DependencyFinder;
import kame.repositories.PackageVersion;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

public class PluginUpdater {

    public static Optional<PackageVersion> check(Plugin plugin, int group_id, String name) throws IOException {
        var packages = new URL(DependencyFinder.getGroupPackageUrl(group_id));
        return DependencyFinder.getLatestVersion(packages, name).filter(x -> !x.version().equals(plugin.getDescription().getVersion()));
    }

    public static void download(PackageVersion pack, File file) throws IOException {
        var url = new URL(DependencyFinder.getDownloadUrl(pack.project_id(), pack.groupId(), pack.artifactId(), pack.version()));
        try (var stream = url.openStream()) {
            var path = Path.of("plugins", Bukkit.getUpdateFolder(), file.getName());
            Files.createDirectories(path.getParent());
            Files.copy(stream, path, StandardCopyOption.REPLACE_EXISTING);
        }
    }
}
